package bazarbazaraps.meme_sb

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.icu.math.BigDecimal


class MyDBHandler(context: Context, name: String?,
                  factory: SQLiteDatabase.CursorFactory?, version: Int) :
        SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {


    companion object {
        //  lateinit var MyDBHandler.Conte
        private var DATABASE_VERSION = 1
        private var DATABASE_NAME = "fav.db"
        var TABLE_FAV = "fav"
        var COLUMN_ID = "_id"
        var COLUMN_FAVSAMPLERNAME = "favsamplename"
        var COLUMN_FAVNAME = "favname"

        // val instance: MyDBHandler by lazy{ MyDBHandler(this ,null, null, 1)}
    }



    override fun onCreate(db: SQLiteDatabase){
        val CREATE_FAV_TABLE = ("CREATE TABLE " +
                TABLE_FAV + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY," +
                COLUMN_FAVSAMPLERNAME + " TEXT  unique," +
                COLUMN_FAVNAME + " TEXT" +")")





        db.execSQL(CREATE_FAV_TABLE)
    }
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int){

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAV)
        onCreate(db)
    }
    fun addWorker(fav_table:String,fav_names_table:String){
        val values = ContentValues()
        values.put(COLUMN_FAVSAMPLERNAME,fav_table)
        values.put(COLUMN_FAVNAME,fav_names_table)


        val db = this.writableDatabase

        db.insert(TABLE_FAV, null, values)
        db.close()

    }

    fun removeWorker(fav_table:String){

        val db = this.writableDatabase

        db.delete(TABLE_FAV, COLUMN_FAVSAMPLERNAME +"= '"+fav_table+"'",null )
        db.close()

    }
    fun removeWorkerName(samplelistname:String){

        val db = this.writableDatabase

        db.delete(TABLE_FAV, COLUMN_FAVNAME +"= \""+samplelistname+"\"",null )
        db.close()

    }





    fun getListContents(): Cursor {
        val db = this.writableDatabase
        val values = db.rawQuery("SELECT * FROM " + TABLE_FAV, null)

        return  values
    }



}