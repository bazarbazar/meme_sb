package bazarbazaraps.meme_sb

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.Intent.getIntent
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.support.v4.content.ContextCompat.startActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent.getIntent
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent.getIntent
import bazarbazaraps.meme_sb.R
import kotlinx.android.synthetic.main.activity_fav.*
import kotlinx.android.synthetic.main.row_main_fav.*
import com.startapp.android.publish.adsCommon.StartAppAd
import com.startapp.android.publish.adsCommon.StartAppSDK
class FavActivity : AppCompatActivity() {
    private lateinit var mp: MediaPlayer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fav)
       // StartAppSDK.init(this, "200012265", true);
        StartAppSDK.init(this, "123", true);





        main_button.setOnClickListener(){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            StartAppAd.showAd(this)
        }

//AD
                val permissions  = Permissions()

                var fav_table = ArrayList<String>()
                var fav_names_table = ArrayList<String>()

        var dbhandler = MyDBHandler(this, null, null, 1)
        val data = dbhandler.getListContents()

        var numRows = data.count
        if (numRows==0){

        }else{
            var i =0
            while(data.moveToNext()){

                fav_table.add(data.getString(1))
                fav_names_table.add(data.getString(2))
                i++
            }

        }





                val  sample_names_const = fav_table
                val  sample_names_const_on_list = fav_names_table
                val  MP_instances = ArrayList<Int>()

         val listView = findViewById<ListView>(R.id.fav_list_view)
         listView.adapter = MyCustomAdapter(this, fav_names_table)

        for (item in fav_table){

            var fav_item = resources.getIdentifier(item,"raw","$packageName")
            MP_instances.add(fav_item)
        }
//



                val mediaPlayer = MediaPlayerSingleton.instance
                //   mp = MediaPlayer.create (this, R.raw.aids)



                fun share_audio( file_path: String ){
                    val audio =  Uri.parse(file_path)

                    val shareIntent = Intent()
                    shareIntent.action = Intent.ACTION_SEND
                    shareIntent.type = "audio/mpeg"
                    shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                    shareIntent.putExtra(Intent.EXTRA_STREAM, audio);

                    startActivity(Intent.createChooser(shareIntent, "Meme Sound share..."))

                }

                fun save_to_Downloads(sample_raw: Int, sample_name: String){

                    permissions.setupPermissions(this)
                    val raw1 = resources.openRawResource(sample_raw)
                    val dataraw1= raw1.readBytes()


                    var file1: File
                    val outputStream1: FileOutputStream
                    try {
                        file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS  ),"/"+ "MemeSB")
                        if (!file1.exists()) {
                            file1.mkdir();
                        }
                        file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS  +"/"+ "MemeSB"), sample_name +".mp3")
                        outputStream1 = FileOutputStream(file1)
                        outputStream1.write(dataraw1)
                        outputStream1.close()
                        Toast.makeText(this,"File "+sample_name +".mp3 succesfully saved in: Downloads/MemeSB/"  , Toast.LENGTH_LONG).show()
                    } catch (e: IOException) {
                        e.printStackTrace()
                        Toast.makeText(this,"Opsss something goes wrong..."  , Toast.LENGTH_LONG).show()
                    }

                }

                fun set_ringtone(sample_raw: Int,sample_name: String){
                    permissions.setupPermissions(this)

                    // val path = Uri.parse("android.resource://bazarbazaraps.rickapp/raw/"+ sample_name)

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.System.canWrite(applicationContext)) {
                            val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:$packageName"))
                            startActivityForResult(intent, 200)

                        }
                        if (Settings.System.canWrite(applicationContext)) {
                            val raw1 = resources.openRawResource(sample_raw)
                            val dataraw1= raw1.readBytes()


                            var file1: File
                            val outputStream1: FileOutputStream
                            try {

                                file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_RINGTONES),sample_name +".mp3")
                                outputStream1 = FileOutputStream(file1)
                                outputStream1.write(dataraw1)
                                outputStream1.close()
                                Toast.makeText(this,"File "+sample_name +".mp3 succesfully saved in: Ringtones/", Toast.LENGTH_LONG).show()
                                //SET RINGTONE
                                val values =  ContentValues()
                                values.put(MediaStore.MediaColumns.DATA, file1.getAbsolutePath());
                                values.put(MediaStore.MediaColumns.TITLE, file1.getName());
                                values.put(MediaStore.MediaColumns.SIZE, file1.length());
                                values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
                                values.put(MediaStore.Audio.AudioColumns.ARTIST, this.getString(R.string.app_name));
                                values.put(MediaStore.Audio.AudioColumns.IS_RINGTONE, true);
                                values.put(MediaStore.Audio.AudioColumns.IS_NOTIFICATION, false);
                                values.put(MediaStore.Audio.AudioColumns.IS_ALARM, false);
                                values.put(MediaStore.Audio.AudioColumns.IS_MUSIC, false);
                                var uri = MediaStore.Audio.Media.getContentUriForPath(file1.getAbsolutePath());
                                this.getContentResolver().delete(uri, MediaStore.MediaColumns.DATA + "=\"" + file1.getAbsolutePath() + "\"", null);

                                //Ok now insert it
                                var newUri = this.getContentResolver().insert(uri, values);
                                RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_RINGTONE, newUri);
                                Toast.makeText(this,"File "+sample_name +".mp3 succesfully set as RINGTONE"  , Toast.LENGTH_LONG).show()

                            } catch (e: IOException) {
                                e.printStackTrace()
                                Toast.makeText(this,"Opsss something goes wrong..."  , Toast.LENGTH_LONG).show()
                            }

                        }
                    }


                }

                fun set_notification(sample_raw: Int,sample_name: String){
                    permissions.setupPermissions(this)

                    // val path = Uri.parse("android.resource://bazarbazaraps.rickapp/raw/"+ sample_name)

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.System.canWrite(applicationContext)) {
                            val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:$packageName"))
                            startActivityForResult(intent, 200)

                        }
                        if (Settings.System.canWrite(applicationContext)) {
                            val raw1 = resources.openRawResource(sample_raw)
                            val dataraw1= raw1.readBytes()


                            var file1: File
                            val outputStream1: FileOutputStream
                            try {

                                file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_NOTIFICATIONS),sample_name +".mp3")
                                outputStream1 = FileOutputStream(file1)
                                outputStream1.write(dataraw1)
                                outputStream1.close()
                                Toast.makeText(this,"File "+sample_name +".mp3 succesfully saved in: Notifications/", Toast.LENGTH_LONG).show()
                                //SET RINGTONE
                                val values =  ContentValues()
                                values.put(MediaStore.MediaColumns.DATA, file1.getAbsolutePath());
                                values.put(MediaStore.MediaColumns.TITLE, file1.getName());
                                values.put(MediaStore.MediaColumns.SIZE, file1.length());
                                values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
                                values.put(MediaStore.Audio.AudioColumns.ARTIST, this.getString(R.string.app_name));
                                values.put(MediaStore.Audio.AudioColumns.IS_RINGTONE, false);
                                values.put(MediaStore.Audio.AudioColumns.IS_NOTIFICATION, true);
                                values.put(MediaStore.Audio.AudioColumns.IS_ALARM, false);
                                values.put(MediaStore.Audio.AudioColumns.IS_MUSIC, false);
                                var uri = MediaStore.Audio.Media.getContentUriForPath(file1.getAbsolutePath());
                                this.getContentResolver().delete(uri, MediaStore.MediaColumns.DATA + "=\"" + file1.getAbsolutePath() + "\"", null);

                                //Ok now insert it
                                var newUri = this.getContentResolver().insert(uri, values);
                                RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_NOTIFICATION, newUri);
                                Toast.makeText(this,"File "+sample_name +".mp3 succesfully set as NOTIFICATION"  , Toast.LENGTH_LONG).show()

                            } catch (e: IOException) {
                                e.printStackTrace()
                                Toast.makeText(this,"Opsss something goes wrong..."  , Toast.LENGTH_LONG).show()
                            }

                        }
                    }


                }

                fun set_alarm(sample_raw: Int,sample_name: String){

                    permissions.setupPermissions(this)
                    // val path = Uri.parse("android.resource://bazarbazaraps.rickapp/raw/"+ sample_name)

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.System.canWrite(applicationContext)) {
                            val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:$packageName"))
                            startActivityForResult(intent, 200)

                        }
                        if (Settings.System.canWrite(applicationContext)) {
                            val raw1 = resources.openRawResource(sample_raw)
                            val dataraw1= raw1.readBytes()


                            var file1: File
                            val outputStream1: FileOutputStream
                            try {

                                file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_ALARMS),sample_name +".mp3")
                                outputStream1 = FileOutputStream(file1)
                                outputStream1.write(dataraw1)
                                outputStream1.close()
                                Toast.makeText(this,"File "+sample_name +".mp3 succesfully saved in: Alarms/", Toast.LENGTH_LONG).show()
                                //SET RINGTONE
                                val values =  ContentValues()
                                values.put(MediaStore.MediaColumns.DATA, file1.getAbsolutePath());
                                values.put(MediaStore.MediaColumns.TITLE, file1.getName());
                                values.put(MediaStore.MediaColumns.SIZE, file1.length());
                                values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
                                values.put(MediaStore.Audio.AudioColumns.ARTIST, this.getString(R.string.app_name));
                                values.put(MediaStore.Audio.AudioColumns.IS_RINGTONE, false);
                                values.put(MediaStore.Audio.AudioColumns.IS_NOTIFICATION, false);
                                values.put(MediaStore.Audio.AudioColumns.IS_ALARM, true);
                                values.put(MediaStore.Audio.AudioColumns.IS_MUSIC, false);
                                var uri = MediaStore.Audio.Media.getContentUriForPath(file1.getAbsolutePath());
                                this.getContentResolver().delete(uri, MediaStore.MediaColumns.DATA + "=\"" + file1.getAbsolutePath() + "\"", null);

                                //Ok now insert it
                                var newUri = this.getContentResolver().insert(uri, values);
                                RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_ALARM, newUri);
                                Toast.makeText(this,"File "+sample_name +".mp3 succesfully set as ALARM"  , Toast.LENGTH_LONG).show()

                            } catch (e: IOException) {
                                e.printStackTrace()
                                Toast.makeText(this,"Opsss something goes wrong..."  , Toast.LENGTH_LONG).show()
                            }

                        }
                    }


                }

                listView.setOnItemClickListener {
                    parent, view, position, id ->


                    mediaPlayer.reset_sound(MP_instances[position],this)

                    mediaPlayer.MPplay( MP_instances[position],this)




                }

                listView.setOnItemLongClickListener {
                    parent, view, position, id ->
                    //share_audio("android.resource://bazarbazaraps.rickapp/raw/"+ sample_names_const[position])
                    val popupMenu = PopupMenu(this,view)
                    popupMenu.setOnMenuItemClickListener {item ->
                        when(item.itemId){
                            R.id.ItemSHARE ->{
                                share_audio("android.resource://bazarbazaraps.meme_sb/raw/"+ sample_names_const[position])
                                true

                            }
                            R.id.ItemSAVE ->{
                                save_to_Downloads(MP_instances[position],sample_names_const[position])
                                true
                            }
                            R.id.ItemRINGTONE ->{
                                set_ringtone(MP_instances[position],sample_names_const[position])
                                true
                            }
                            R.id.ItemNOTIFICATION ->{
                                set_notification(MP_instances[position],sample_names_const[position])
                                true
                            }
                            R.id.ItemALARM ->{
                                set_alarm(MP_instances[position],sample_names_const[position])
                                true
                            }
                            else -> false
                        }
                    }
                    popupMenu.inflate(R.menu.popup_menu)
                    popupMenu.show()
                    true
                }


            }
            private class MyCustomAdapter(context: Context?, private var sample_names: ArrayList<String>) : ArrayAdapter<String>(context, -1, sample_names){


                private var mContext = context
                init{
                    mContext = context
                }
                override fun getCount(): Int {
                    return sample_names.size
                }
                override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
                    //val textView = TextView(mContext)
                    //textView.text="row"
                    // return textView


                    val layoutInflater = LayoutInflater.from(mContext)
                    val rowMain= layoutInflater.inflate(R.layout.row_main_fav, parent, false)
                    val sampleTextView = rowMain.findViewById<TextView>(R.id.sampletextView)
                    sampleTextView.text = sample_names[position]

                    var add2fav = rowMain.findViewById<Button>(R.id.fav_button) as ToggleButton
                    add2fav.isChecked = true

                    add2fav.setOnCheckedChangeListener { buttonView, isChecked ->
                        if (isChecked==false) {
                            var dbhandler = MyDBHandler(context, null, null, 1)

                                //Toast.makeText(mContext, "huj" + (position).toString(), Toast.LENGTH_LONG).show()
                                //fav_table.fav_table.add(sample_names_const[position])
                                //fav_table.fav_names_table.add(sample_names[position])
                                dbhandler.removeWorkerName(sample_names[position])
                            sample_names.removeAt(position)
                            this.notifyDataSetChanged()
                        }
                    }




                    return rowMain
                }




                override fun getItemId(position: Int): Long {
                    return position.toLong()
                }

                override fun getItem(position: Int): String? {
                    return "test"
                }



            }



    override fun onBackPressed() {
        StartAppAd.onBackPressed(this);
        super.onBackPressed();
    }
        }









