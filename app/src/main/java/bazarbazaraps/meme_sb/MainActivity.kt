package bazarbazaraps.meme_sb

import android.app.AlarmManager
import android.app.Notification
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.media.MediaPlayer
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.view.*
import android.widget.*
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import android.content.ContentValues
import android.icu.math.BigDecimal
import android.media.RingtoneManager
import android.os.Build
import android.provider.MediaStore
import android.provider.Settings
import android.support.v4.app.AlarmManagerCompat
import android.view.KeyEvent
import bazarbazaraps.meme_sb.R
import bazarbazaraps.meme_sb.R.id.fav_button
import kotlinx.android.synthetic.main.activity_main.*
import com.startapp.android.publish.adsCommon.StartAppAd
import com.startapp.android.publish.adsCommon.StartAppSDK

class MainActivity : AppCompatActivity() {

    private lateinit var mp: MediaPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//AD
        //StartAppSDK.init(this, "200012265", true);
        StartAppSDK.init(this, "123", true);
     //  mInterstitialAd.adUnitId = "ca-app-pub-3940256099942544/1033173712"

        mfav_button.setOnClickListener(){
            val intent = Intent(this, FavActivity::class.java)
            startActivity(intent)
        }
//AD
        val permissions  = Permissions()

/*
aligator
antelope
bear
bison
canary
cat
chicken
cow
dog
dolphin
donkey
duck
eagle
fox
frog
goat
hippopotamus
horse
lion
panda
parrot
pig
puma
rooster
monkey
sheep
snake
tiger
turkey
wolf







*/
          val  sample_names_const = arrayListOf<String>(
                "a100",
                "ased4airhorn",
                "airhorn",
                "airhornsonata",
                "allahuakbar",
                "ballsofsteel",
                "batmanondrugs",
                "bruhsoundeffect",
                "buzzerwronganswer",
                "canttiemyshoes",
                "crickets",
                "cykablyat",
                "damnson",
                "darudeflutestorm",
                "darudedankstorm",
                "deeznutssoundeffect",
                "dejavu",
                "douknowdaway",
                "dundundun",
                "epicsaxguy",
                "fart",
                "funnylaugh",
                "getnoscoped",
                "getyourhands",
                "gothim",
                "hagaysoundeffect",
                "heavenlymusic",
                "hellodarknessmyoldfriend",
                "helpmehelpme",
                "hereitcomes",
                "hothothot",
                "huwaawaa",
                "iamtheone",
                "idontgiveafuck",
                "illuminaticonfirmed",
                "justdoit",
                "kazookid",
                "leedleleedleleedlelee",
                "leeroyjenkins",
                "megafaggot",
                "missionfailedwellgetemnexttime",
                "missmewiththat",
                "momgetthecamera",
                "mydickfelloff",
                "mynameisjeff",
                "nein",
                "nogodpleaseno",
                "ohbabyatriple",
                "okokokok",
                "punch",
                "racist",
                "reallynigga",
                "restinpieces",
                "runninginthe90s",
                "sadtrombone",
                "sanic",
                "shotsfired",
                "shutyourbitchassup",
                "shutyourfuckingmouth",
                "sipapi",
                "smokeweedeveryday",
                "suprisemotherfcker",
                "tadaah",
                "takeonmeflute",
                "thuglife",
                "titanicflute",
                "trololo",
                "trymebitch",
                "vine",
                "vomit",
                "wastedbusted",
                "whatarethose",
                "whatdidyousay",
                "whatthefuckisthis",
                "whatthefuckwasthatshit",
                "whothefuck",
                "wombocombo",
                "wow",
                "wtf",
                "yeeeah",
                "youlooklikemufuckinguhhhhhh",
                "youneedtoshutthefuckup",
                "yousmart"



        )

            val  MP_instances = ArrayList<Int>()
//var x = ("R.rav."+sample_names_const[0]).toInt()
        MP_instances.add(R.raw.a100)
        MP_instances.add(R.raw.ased4airhorn)
        MP_instances.add(R.raw.airhorn)
        MP_instances.add(R.raw.airhornsonata)
        MP_instances.add(R.raw.allahuakbar)
        MP_instances.add(R.raw.ballsofsteel)
        MP_instances.add(R.raw.batmanondrugs)
        MP_instances.add(R.raw.bruhsoundeffect)
        MP_instances.add(R.raw.buzzerwronganswer)
        MP_instances.add(R.raw.canttiemyshoes)
        MP_instances.add(R.raw.crickets)
        MP_instances.add(R.raw.cykablyat)
        MP_instances.add(R.raw.damnson)
        MP_instances.add(R.raw.darudeflutestorm)
        MP_instances.add(R.raw.darudedankstorm)
        MP_instances.add(R.raw.deeznutssoundeffect)
        MP_instances.add(R.raw.dejavu)
        MP_instances.add(R.raw.douknowdaway)
        MP_instances.add(R.raw.dundundun)
        MP_instances.add(R.raw.epicsaxguy)
        MP_instances.add(R.raw.fart)
        MP_instances.add(R.raw.funnylaugh)
        MP_instances.add(R.raw.getnoscoped)
        MP_instances.add(R.raw.getyourhands)
        MP_instances.add(R.raw.gothim)
        MP_instances.add(R.raw.hagaysoundeffect)
        MP_instances.add(R.raw.heavenlymusic)
        MP_instances.add(R.raw.hellodarknessmyoldfriend)
        MP_instances.add(R.raw.helpmehelpme)
        MP_instances.add(R.raw.hereitcomes)
        MP_instances.add(R.raw.hothothot)
        MP_instances.add(R.raw.huwaawaa)
        MP_instances.add(R.raw.iamtheone)
        MP_instances.add(R.raw.idontgiveafuck)
        MP_instances.add(R.raw.illuminaticonfirmed)
        MP_instances.add(R.raw.justdoit)
        MP_instances.add(R.raw.kazookid)
        MP_instances.add(R.raw.leedleleedleleedlelee)
        MP_instances.add(R.raw.leeroyjenkins)
        MP_instances.add(R.raw.megafaggot)
        MP_instances.add(R.raw.missionfailedwellgetemnexttime)
        MP_instances.add(R.raw.missmewiththat)
        MP_instances.add(R.raw.momgetthecamera)
        MP_instances.add(R.raw.mydickfelloff)
        MP_instances.add(R.raw.mynameisjeff)
        MP_instances.add(R.raw.nein)
        MP_instances.add(R.raw.nogodpleaseno)
        MP_instances.add(R.raw.ohbabyatriple)
        MP_instances.add(R.raw.okokokok)
        MP_instances.add(R.raw.punch)
        MP_instances.add(R.raw.racist)
        MP_instances.add(R.raw.reallynigga)
        MP_instances.add(R.raw.restinpieces)
        MP_instances.add(R.raw.runninginthe90s)
        MP_instances.add(R.raw.sadtrombone)
        MP_instances.add(R.raw.sanic)
        MP_instances.add(R.raw.shotsfired)
        MP_instances.add(R.raw.shutyourbitchassup)
        MP_instances.add(R.raw.shutyourfuckingmouth)
        MP_instances.add(R.raw.sipapi)
        MP_instances.add(R.raw.smokeweedeveryday)
        MP_instances.add(R.raw.suprisemotherfcker)
        MP_instances.add(R.raw.tadaah)
        MP_instances.add(R.raw.takeonmeflute)
        MP_instances.add(R.raw.thuglife)
        MP_instances.add(R.raw.titanicflute)
        MP_instances.add(R.raw.trololo)
        MP_instances.add(R.raw.trymebitch)
        MP_instances.add(R.raw.vine)
        MP_instances.add(R.raw.vomit)
        MP_instances.add(R.raw.wastedbusted)
        MP_instances.add(R.raw.whatarethose)
        MP_instances.add(R.raw.whatdidyousay)
        MP_instances.add(R.raw.whatthefuckisthis)
        MP_instances.add(R.raw.whatthefuckwasthatshit)
        MP_instances.add(R.raw.whothefuck)
        MP_instances.add(R.raw.wombocombo)
        MP_instances.add(R.raw.wow)
        MP_instances.add(R.raw.wtf)
        MP_instances.add(R.raw.yeeeah)
        MP_instances.add(R.raw.youlooklikemufuckinguhhhhhh)
        MP_instances.add(R.raw.youneedtoshutthefuckup)
        MP_instances.add(R.raw.yousmart)


            val listView = findViewById<ListView>(R.id.samples_list_view)
            listView.adapter = MyCustomAdapter(this, sample_names_const)

            val mediaPlayer = MediaPlayerSingleton.instance




            fun share_audio( file_path: String ){
                val audio =  Uri.parse(file_path)

                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type = "audio/mpeg"
                shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                shareIntent.putExtra(Intent.EXTRA_STREAM, audio);

                startActivity(Intent.createChooser(shareIntent, "Meme SB share..."))

            }

            fun save_to_Downloads(sample_raw: Int, sample_name: String){

                permissions.setupPermissions(this)
                val raw1 = resources.openRawResource(sample_raw)
                val dataraw1= raw1.readBytes()


                var file1: File
                val outputStream1: FileOutputStream
                try {
                    file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS  ),"/"+ "MemeSB")
                if (!file1.exists()) {
                    file1.mkdir();
                }
                file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS  +"/"+ "MemeSB"), sample_name +".mp3")
                outputStream1 = FileOutputStream(file1)
                outputStream1.write(dataraw1)
                outputStream1.close()
                Toast.makeText(this,"File "+sample_name +".mp3 succesfully saved in: Downloads/MemeSB/"  ,Toast.LENGTH_LONG).show()
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this,"Opsss something goes wrong..."  ,Toast.LENGTH_LONG).show()
                }

            }

            fun set_ringtone(sample_raw: Int,sample_name: String){
                permissions.setupPermissions(this)

            // val path = Uri.parse("android.resource://bazarbazaraps.rickapp/raw/"+ sample_name)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Settings.System.canWrite(applicationContext)) {
                        val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:$packageName"))
                        startActivityForResult(intent, 200)

                    }
                    if (Settings.System.canWrite(applicationContext)) {
                        val raw1 = resources.openRawResource(sample_raw)
                        val dataraw1= raw1.readBytes()


                        var file1: File
                        val outputStream1: FileOutputStream
                        try {

                            file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_RINGTONES),sample_name +".mp3")
                            outputStream1 = FileOutputStream(file1)
                            outputStream1.write(dataraw1)
                            outputStream1.close()
                            Toast.makeText(this,"File "+sample_name +".mp3 succesfully saved in: Ringtones/",Toast.LENGTH_LONG).show()
                            //SET RINGTONE
                            val values =  ContentValues()
                            values.put(MediaStore.MediaColumns.DATA, file1.getAbsolutePath());
                            values.put(MediaStore.MediaColumns.TITLE, file1.getName());
                            values.put(MediaStore.MediaColumns.SIZE, file1.length());
                            values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
                            values.put(MediaStore.Audio.AudioColumns.ARTIST, this.getString(R.string.app_name));
                            values.put(MediaStore.Audio.AudioColumns.IS_RINGTONE, true);
                            values.put(MediaStore.Audio.AudioColumns.IS_NOTIFICATION, false);
                            values.put(MediaStore.Audio.AudioColumns.IS_ALARM, false);
                            values.put(MediaStore.Audio.AudioColumns.IS_MUSIC, false);
                            var uri = MediaStore.Audio.Media.getContentUriForPath(file1.getAbsolutePath());
                            this.getContentResolver().delete(uri, MediaStore.MediaColumns.DATA + "=\"" + file1.getAbsolutePath() + "\"", null);

                            //Ok now insert it
                            var newUri = this.getContentResolver().insert(uri, values);
                            RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_RINGTONE, newUri);
                            Toast.makeText(this,"File "+sample_name +".mp3 succesfully set as RINGTONE"  ,Toast.LENGTH_LONG).show()

                        } catch (e: IOException) {
                            e.printStackTrace()
                            Toast.makeText(this,"Opsss something goes wrong..."  ,Toast.LENGTH_LONG).show()
                        }

                    }
                }


            }

            fun set_notification(sample_raw: Int,sample_name: String){
                permissions.setupPermissions(this)

                // val path = Uri.parse("android.resource://bazarbazaraps.rickapp/raw/"+ sample_name)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Settings.System.canWrite(applicationContext)) {
                        val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:$packageName"))
                        startActivityForResult(intent, 200)

                    }
                    if (Settings.System.canWrite(applicationContext)) {
                        val raw1 = resources.openRawResource(sample_raw)
                        val dataraw1= raw1.readBytes()


                        var file1: File
                        val outputStream1: FileOutputStream
                        try {

                            file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_NOTIFICATIONS),sample_name +".mp3")
                            outputStream1 = FileOutputStream(file1)
                            outputStream1.write(dataraw1)
                            outputStream1.close()
                            Toast.makeText(this,"File "+sample_name +".mp3 succesfully saved in: Notifications/",Toast.LENGTH_LONG).show()
                            //SET RINGTONE
                            val values =  ContentValues()
                            values.put(MediaStore.MediaColumns.DATA, file1.getAbsolutePath());
                            values.put(MediaStore.MediaColumns.TITLE, file1.getName());
                            values.put(MediaStore.MediaColumns.SIZE, file1.length());
                            values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
                            values.put(MediaStore.Audio.AudioColumns.ARTIST, this.getString(R.string.app_name));
                            values.put(MediaStore.Audio.AudioColumns.IS_RINGTONE, false);
                            values.put(MediaStore.Audio.AudioColumns.IS_NOTIFICATION, true);
                            values.put(MediaStore.Audio.AudioColumns.IS_ALARM, false);
                            values.put(MediaStore.Audio.AudioColumns.IS_MUSIC, false);
                            var uri = MediaStore.Audio.Media.getContentUriForPath(file1.getAbsolutePath());
                            this.getContentResolver().delete(uri, MediaStore.MediaColumns.DATA + "=\"" + file1.getAbsolutePath() + "\"", null);

                            //Ok now insert it
                            var newUri = this.getContentResolver().insert(uri, values);
                            RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_NOTIFICATION, newUri);
                            Toast.makeText(this,"File "+sample_name +".mp3 succesfully set as NOTIFICATION"  ,Toast.LENGTH_LONG).show()

                        } catch (e: IOException) {
                            e.printStackTrace()
                            Toast.makeText(this,"Opsss something goes wrong..."  ,Toast.LENGTH_LONG).show()
                        }

                    }
                }


            }

            fun set_alarm(sample_raw: Int,sample_name: String){

                permissions.setupPermissions(this)
                // val path = Uri.parse("android.resource://bazarbazaraps.rickapp/raw/"+ sample_name)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Settings.System.canWrite(applicationContext)) {
                        val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:$packageName"))
                        startActivityForResult(intent, 200)

                    }
                    if (Settings.System.canWrite(applicationContext)) {
                        val raw1 = resources.openRawResource(sample_raw)
                        val dataraw1= raw1.readBytes()


                        var file1: File
                        val outputStream1: FileOutputStream
                        try {

                            file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_ALARMS),sample_name +".mp3")
                            outputStream1 = FileOutputStream(file1)
                            outputStream1.write(dataraw1)
                            outputStream1.close()
                            Toast.makeText(this,"File "+sample_name +".mp3 succesfully saved in: Alarms/",Toast.LENGTH_LONG).show()
                            //SET RINGTONE
                            val values =  ContentValues()
                            values.put(MediaStore.MediaColumns.DATA, file1.getAbsolutePath());
                            values.put(MediaStore.MediaColumns.TITLE, file1.getName());
                            values.put(MediaStore.MediaColumns.SIZE, file1.length());
                            values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
                            values.put(MediaStore.Audio.AudioColumns.ARTIST, this.getString(R.string.app_name));
                            values.put(MediaStore.Audio.AudioColumns.IS_RINGTONE, false);
                            values.put(MediaStore.Audio.AudioColumns.IS_NOTIFICATION, false);
                            values.put(MediaStore.Audio.AudioColumns.IS_ALARM, true);
                            values.put(MediaStore.Audio.AudioColumns.IS_MUSIC, false);
                            var uri = MediaStore.Audio.Media.getContentUriForPath(file1.getAbsolutePath());
                            this.getContentResolver().delete(uri, MediaStore.MediaColumns.DATA + "=\"" + file1.getAbsolutePath() + "\"", null);

                            //Ok now insert it
                            var newUri = this.getContentResolver().insert(uri, values);
                            RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_ALARM, newUri);
                            Toast.makeText(this,"File "+sample_name +".mp3 succesfully set as ALARM"  ,Toast.LENGTH_LONG).show()

                        } catch (e: IOException) {
                            e.printStackTrace()
                            Toast.makeText(this,"Opsss something goes wrong..."  ,Toast.LENGTH_LONG).show()
                        }

                    }
                }


            }

            listView.setOnItemClickListener {
                parent, view, position, id ->


                mediaPlayer.reset_sound(MP_instances[position],this)

                mediaPlayer.MPplay( MP_instances[position],this)




            }

            listView.setOnItemLongClickListener {
                parent, view, position, id ->
               //share_audio("android.resource://bazarbazaraps.rickapp/raw/"+ sample_names_const[position])
                val popupMenu = PopupMenu(this,view)
                popupMenu.setOnMenuItemClickListener {item ->
                    when(item.itemId){
                        R.id.ItemSHARE ->{
                        share_audio("android.resource://bazarbazaraps.meme_sb/raw/"+ sample_names_const[position])
                            true

                        }
                        R.id.ItemSAVE ->{
                            save_to_Downloads(MP_instances[position],sample_names_const[position])
                            true
                        }
                        R.id.ItemRINGTONE ->{
                            set_ringtone(MP_instances[position],sample_names_const[position])
                            true
                        }
                        R.id.ItemNOTIFICATION ->{
                            set_notification(MP_instances[position],sample_names_const[position])
                            true
                        }
                        R.id.ItemALARM ->{
                            set_alarm(MP_instances[position],sample_names_const[position])
                            true
                        }
                        else -> false
                    }
                }
               popupMenu.inflate(R.menu.popup_menu)
               popupMenu.show()
              true
            }


        }



    override fun onStop() {
        val mediaPlayer = MediaPlayerSingleton.instance
        //mediaPlayer.mp.stop()
//        mediaPlayer.mp.reset()
        //mediaPlayer.mp.release()
                super.onStop()
    }
    private class MyCustomAdapter(context: Context?, private val sample_names_const: ArrayList<String>) : ArrayAdapter<String>(context, -1, sample_names_const){



        private val  sample_names = arrayListOf<String>(
                "100",
                "2SED4AIRHORN",
                "AIRHORN SONATA",
                "Airhorn",
                "Allahu Akbar",
                "Balls of steel",
                "Batman on Drugs",
                "Bruh",
                "Buzzer Wrong Answer",
                "Cant Tie My Shoes",
                "Crickets",
                "CYKA BLYAT",
                "Damn Son ",
                "Darude Dankstorm",
                "Darude-Flutestorm",
                "Deez Nutst",
                "Deja Vu",
                "do u know da way",
                "Dun Dun Dun",
                "Epic Sax Guy",
                "Fart",
                "Funny laugh",
                "GET NOSCOPED",
                "GET YOUR HANDS",
                "got him",
                "Ha gay sound effect",
                "Heavenly Music",
                "HELLO DARKNESS MY OLD FRIEND",
                "HELP ME HELP ME...",
                "HERE IT COMES",
                "HOT HOT HOT",
                "Hu Waa Waa",
                "I AM THE ONE",
                "I Dont Give A Fuck",
                "Illuminati Confirmed",
                "JUST DO IT",
                "Kazoo Kid",
                "Leedle Leedle Leedle Lee",
                "Leeroy Jenkins",
                "Mega faggot",
                "Miss me with that",
                "Mission Failed well get em next time",
                "Mom Get The Camera",
                "MY DICK FELL OFF",
                "My Name Is Jeff",
                "Nein",
                "No God please no",
                "Oh Baby A Triple",
                "Ok Ok Ok Ok",
                "Punch",
                "RACIST",
                "Really Nigga",
                "Rest in pieces",
                "Running in the 90s",
                "Sad Trombone",
                "SANIC",
                "SHOTS FIRED",
                "Shut Your Bitch Ass Up",
                "Shut Your Fucking Mouth",
                "SI PAPI",
                "Smoke Weed Everyday",
                "Suprise Motherfcker",
                "Tadaah",
                "TAKE ON ME FLUTE",
                "Thug Life",
                "Titanic Flute",
                "Trololo",
                "Try Me Bitch",
                "VINE",
                "Vomit",
                "WastedBusted",
                "What are those",
                "What did you say",
                "what the fuck is this",
                "what the fuck was that shit",
                "who the fuck",
                "Wombo Combo",
                "wow",
                "WTF",
                "Yeeeah",
                "You look like mufucking uhhhhhh",
                "You Need To Shut The Fuck Up",
                "You smart"


        )

            private var mContext = context
            init{
                mContext = context
            }
            override fun getCount(): Int {
                return sample_names.size
            }
            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
               //val textView = TextView(mContext)
                //textView.text="row"
               // return textView



this.notifyDataSetChanged()

                val layoutInflater = LayoutInflater.from(mContext)
                val rowMain= layoutInflater.inflate(R.layout.row_main, parent, false)
                val sampleTextView = rowMain.findViewById<TextView>(R.id.sampletextView)
                sampleTextView.text = sample_names[position]
                var add2fav = rowMain.findViewById<Button>(fav_button) as ToggleButton
                    var fav_table = ArrayList<String>()

                    var dbhandler = MyDBHandler(context, null, null, 1)
                    val data = dbhandler.getListContents()
                    add2fav.isChecked = false
                    var numRows = data.count
                    if (numRows==0){

                        add2fav.isChecked = false

                    }else {
                        var i = 0
                        add2fav.isChecked = false
                        while (data.moveToNext()) {
//
                            if (data.getString(1) == sample_names_const[position]) {
                                add2fav.isChecked = true
                            }
//
                        }
                    }
             //


                add2fav.setOnCheckedChangeListener{buttonView, isChecked ->
                    if (isChecked) {

                        if (fav_table.size == 0) {
                            //Toast.makeText(mContext, "huj" + (position + 1).toString(), Toast.LENGTH_LONG).show()
                            //fav_table.fav_table.add(sample_names_const[position])
                            //fav_table.fav_names_table.add(sample_names[position])
                            dbhandler.addWorker(sample_names_const[position], sample_names[position])
                        } else {
                            var unique = true
                            for (item in fav_table) {
                                if (item == sample_names_const[position]) {
                                    unique = false
                                }

                            }
                            if (unique == true) {
                                //fav_table.fav_table.add(sample_names_const[position])
                                //fav_table.fav_names_table.add(sample_names[position])
                                dbhandler.addWorker(sample_names_const[position], sample_names[position])

                            }
                        }

                    }else{
                        //Toast.makeText(mContext, "hujwwwww" + (position + 1).toString(), Toast.LENGTH_LONG).show()

                        dbhandler.removeWorker(sample_names_const[position])
                    }
                }

                /*
                add2fav.setOnClickListener(object :  View.OnClickListener {


                    override fun onClick(v: View?) {

                        if (checked) {

                            if (fav_table.fav_table.size == 0) {
                                Toast.makeText(mContext, "huj" + (position + 1).toString(), Toast.LENGTH_LONG).show()
                                //fav_table.fav_table.add(sample_names_const[position])
                                //fav_table.fav_names_table.add(sample_names[position])
                                dbhandler.addWorker(sample_names_const[position], sample_names[position])
                            } else {
                                var unique = true
                                for (item in fav_table.fav_table) {
                                    if (item == sample_names_const[position]) {
                                        unique = false
                                    }

                                }
                                if (unique == true) {
                                    //fav_table.fav_table.add(sample_names_const[position])
                                    //fav_table.fav_names_table.add(sample_names[position])
                                    dbhandler.addWorker(sample_names_const[position], sample_names[position])

                                }
                            }
                            checked = false
                        }else{
                            Toast.makeText(mContext, "hujwwwww" + (position + 1).toString(), Toast.LENGTH_LONG).show()
                            checked = true
                        }
                    }
                })*/


                return rowMain
            }

            override fun getItemId(position: Int): Long {
                return position.toLong()
            }

            override fun getItem(position: Int): String? {
                return "test"
            }
           


        }




    }







